
/*! @file
 *  This is an example of the PIN tool that demonstrates some basic PIN APIs 
 *  and could serve as the starting point for developing your first PIN tool
 */

#include "pin.H"
#include <iostream>
#include <fstream>
#include <set>
#include <climits>

/* ================================================================== */
// Global variables 
/* ================================================================== */

UINT64 insCount = 0;        //number of dynamically executed instructions
UINT64 bblCount = 0;        //number of dynamically executed basic blocks
UINT64 threadCount = 0;     //total number of threads, including main thread

UINT64 prev = 0;

std::ostream * out = &cerr;

UINT32 data_footprint = 0;
UINT32 ins_footprint = 0;

std::set<int> ins_chunk;
std::set<int> data_chunk;

/* ===================================================================== */
// Command line switches
/* ===================================================================== */

KNOB<UINT64> FastForwardCount(KNOB_MODE_WRITEONCE, "pintool",
    "f", "0", "specify fast forward count in billions");

KNOB<string> KnobOutputFile(KNOB_MODE_WRITEONCE,  "pintool",
    "o", "", "specify file name for MyPinTool output");

KNOB<BOOL>   KnobCount(KNOB_MODE_WRITEONCE,  "pintool",
    "count", "1", "count instructions, basic blocks and threads in the application");


UINT64 INScount[17] = {0};

UINT64 TotalInsToConsider = 1000000000;
UINT64 fast_forward_count;  // Should be a command line input to your PIN tool
UINT64 icount = 0;

UINT32 tempRead, tempWrite, tempMem;
UINT32 memSize;
BOOL IsRead;
BOOL IsWrite;
BOOL IsDirect;
INT32 category;
UINT32 ins_size;
UINT32 numOperands;
UINT32 readReg;
UINT32 writeReg;
UINT32 memOperands;
UINT32 readsize;
UINT32 writesize;
BOOL IsImmediate;
INT32 immediate;
INT64 XXXimmediate;
ADDRDELTA displacement;

UINT64 InsSize[1000] = {0};
UINT64 NumOperandCount[1000] = {0};
UINT64 NumReadRegs[1000] = {0};
UINT64 NumWriteRegs[1000] = {0};
UINT64 NumMemOperandsCount[1000] = {0};
UINT64 NumReadMemOperandsCount[1000] = {0};
UINT64 NumWriteMemOperandsCount[1000] = {0};
UINT64 TotalMemBytes = 0;
UINT64 MaxMemBytes = 0;
UINT64 NumLoadStore = 0;
INT32 MaxImmediate = INT_MIN;
INT32 MinImmediate = INT_MAX;
INT64 XXXMaxImmediate = INT_MIN;
INT64 XXXMinImmediate = INT_MAX;
ADDRDELTA MaxDisplacement = INT_MIN;
ADDRDELTA MinDisplacement = INT_MAX;

void Data_footprint(void * addr, UINT32 size, ADDRDELTA _displacement)
{
    UINT64 data_addr = (UINT64)addr;
    UINT64 a, b, c;

    a = data_addr/32;
    b = (data_addr + size - 1)/32;

    for (c=a; c<=b; c++)
    {
        if(data_chunk.find(c) == data_chunk.end())
        {
            data_chunk.insert(c);
        }
    }

    // if (_displacement > MaxDisplacement) MaxDisplacement = _displacement;
    // if (_displacement < MinDisplacement) MinDisplacement = _displacement;

}

// Analysis routine to check fast-forward condition
ADDRINT FastForward(void)
{
    return (icount >= fast_forward_count && icount < fast_forward_count + TotalInsToConsider);
}

ADDRINT Terminate(void)
{
    return (icount >= fast_forward_count + TotalInsToConsider);
}

// Analysis routine to exit the application
void MyExitRoutine() {
    // Do an exit system call to exit the application.
    // As we are calling the exit system call PIN would not be able to instrument application end.
    // Because of this, even if you are instrumenting the application end, the Fini function would not
    // be called. Thus you should report the statistics here, before doing the exit system call.

    // Results etc

    *out <<  "===============================================" << endl;
    *out <<  "MyPinTool analysis results: " << endl;
    *out <<  "FastForward: " << fast_forward_count  << endl;
    *out <<  "Instruction End: " << icount  << endl;
    //*out <<  "Number of basic blocks: " << bblCount  << endl;
    // *out <<  "Number of threads: " << threadCount  << endl;
    UINT64 i, totalcycles=0, totalins=0;
    for (i=0; i<17; i++){
        *out <<  "Count[" << i << "]: " << INScount[i]  << endl;
        totalins = totalins + INScount[i];
        if (i>1) totalcycles = totalcycles + INScount[i];
        else totalcycles = totalcycles + 50*INScount[i];
    }
    float cpi = (totalcycles*1.0)/totalins;
    *out <<  "CPI: " << cpi  << endl;

    UINT64 ins_footprint_size = 32*ins_chunk.size();
    *out << "Instruction Footprint: " << ins_footprint_size << endl;
    UINT64 data_footprint_size = 32*data_chunk.size();
    *out << "Data Footprint: " << data_footprint_size << endl;

    *out << "Instruction Length: ";
    for(i=0;i<20;i++) *out << InsSize[i] << ", ";
    *out << endl;

    *out << "Number of Operands: ";
    for(i=0;i<10;i++) *out << NumOperandCount[i] << ", ";
    *out << endl;

    *out << "Number of Read Registers: ";
    for(i=0;i<10;i++) *out << NumReadRegs[i] << ", ";
    *out << endl;

    *out << "Number of Write Registers: ";
    for(i=0;i<10;i++) *out << NumWriteRegs[i] << ", ";
    *out << endl;

    *out << "Number of Memory Operands: ";
    for(i=0;i<10;i++) *out << NumMemOperandsCount[i] << ", ";
    *out << endl;

    *out << "Number of Memory Read Operands: ";
    for(i=0;i<10;i++) *out << NumReadMemOperandsCount[i] << ", ";
    *out << endl;

    *out << "Number of Memory Write Operands: ";
    for(i=0;i<10;i++) *out << NumWriteMemOperandsCount[i] << ", ";
    *out << endl;
    
    double AverageBytesTouched = (TotalMemBytes*1.0)/NumLoadStore;
    *out << "Max Bytes Touched: " << MaxMemBytes << endl;
    *out << "Average Bytes Touched: " << AverageBytesTouched << endl;

    *out << "Min Immediate: " << MinImmediate << endl;
    *out << "Max Immediate: " << MaxImmediate << endl;

    *out << "XXX Min Immediate: " << XXXMinImmediate << endl;
    *out << "XXX Max Immediate: " << XXXMaxImmediate << endl;

    *out << "Min Displacement: " << MinDisplacement << endl;
    *out << "Max Displacement: " << MaxDisplacement << endl;

    *out <<  "===============================================" << endl;

    exit(0);
}

void MyPredicatedAnalysis(UINT32 ins_size, UINT32 _memOperands, UINT32 _readsize, UINT32 _writesize, UINT32 _tempMem)
{
    NumMemOperandsCount[_memOperands]++;
    NumReadMemOperandsCount[_readsize]++;
    NumWriteMemOperandsCount[_writesize]++;
    if (_memOperands) NumLoadStore++;
    TotalMemBytes = TotalMemBytes + _tempMem;
    if (_tempMem > MaxMemBytes) MaxMemBytes = _tempMem;
}

void MyAnalysis(void *ip, UINT32 ins_size, UINT32 _numOperands, UINT32 _readReg, UINT32 _writeReg)
{

    UINT64 mem_addr = (UINT64)ip;
    UINT64 a, b, c;

    a = mem_addr/32;
    b = (mem_addr + ins_size - 1)/32;

    for(c=a;c<=b;c++)
    {
        if(ins_chunk.find(c) == ins_chunk.end())
        {

            ins_chunk.insert(c);
        }
    }
    
    // InsSize[ins_size]++;
    // NumOperandCount[_numOperands]++;
    // NumReadRegs[_readReg]++;
    // NumWriteRegs[_writeReg]++;
}

void ProcessImmediate(INT32 _immediate)
{    
    if (_immediate > MaxImmediate) MaxImmediate = _immediate;
    if (_immediate < MinImmediate) MinImmediate = _immediate;
}

void InsCount(){
    icount++;
    if (icount >= prev + 1000000000){
        prev = icount;
        *out <<  "INS: " << icount/1000000000 << endl;
    }
}

void Increment(UINT32 i){
    INScount[i]++;
}

void MemIncrement(UINT32 sizeRead, UINT32 sizeWrite){
    INScount[0] = INScount[0] + sizeRead;
    INScount[1] = INScount[1] + sizeWrite;
}

void Instruction(INS ins, void *v) 
{
    INS_InsertCall(ins, IPOINT_BEFORE, (AFUNPTR) InsCount, IARG_END);

    INS_InsertIfCall(ins, IPOINT_BEFORE, (AFUNPTR) Terminate, IARG_END);
    INS_InsertThenCall(ins, IPOINT_BEFORE, MyExitRoutine, IARG_END);

    IsDirect = INS_IsDirectCall(ins);
    category = INS_Category(ins);
    ins_size = INS_Size(ins);
    numOperands = INS_OperandCount(ins);
    readReg = INS_MaxNumRRegs(ins);
    writeReg = INS_MaxNumWRegs(ins);
    memOperands = INS_MemoryOperandCount(ins);
    readsize = 0;
    writesize = 0;

    tempRead = 0;
    tempWrite = 0;
    tempMem = 0;

    // for(UINT32 insOp = 0; insOp < numOperands; insOp++){
    //     IsImmediate = INS_OperandIsImmediate(ins, insOp);
    //     if (IsImmediate)
    //     {
    //         XXXimmediate = (UINT32)INS_OperandImmediate(ins, insOp);
    //         // *out << "Immediate: " << XXXimmediate << endl;
    //         if (XXXimmediate > XXXMaxImmediate) XXXMaxImmediate = XXXimmediate;
    //         if (XXXimmediate < XXXMinImmediate) XXXMinImmediate = XXXimmediate;

    //         immediate = (UINT32)INS_OperandImmediate(ins, insOp);
    //         INS_InsertIfCall(ins, IPOINT_BEFORE, (AFUNPTR) FastForward, IARG_END);
    //         INS_InsertThenCall(ins, IPOINT_BEFORE, (AFUNPTR) ProcessImmediate, IARG_ADDRINT, immediate, IARG_END);
    //     }

    // }

    for (UINT32 memOp = 0; memOp < memOperands; memOp++) 
    {
    
        memSize = INS_MemoryOperandSize(ins, memOp);
        IsRead = INS_MemoryOperandIsRead(ins, memOp);
        IsWrite = INS_MemoryOperandIsWritten(ins, memOp);
        displacement = INS_OperandMemoryDisplacement(ins, memOp);

        tempMem = tempMem + memSize;

        if(memSize%4 == 0)
            memSize = memSize/4;
        else
            memSize = (memSize/4) + 1; 

        if (IsRead)
        {
            tempRead = tempRead + memSize;
            readsize++;

        }
                
            
        if (IsWrite)
        {
            tempWrite = tempWrite + memSize;
            writesize++;
        }

        INS_InsertIfCall(ins, IPOINT_BEFORE, (AFUNPTR) FastForward, IARG_END);
        INS_InsertThenPredicatedCall(ins, IPOINT_BEFORE, (AFUNPTR)Data_footprint, IARG_INST_PTR, IARG_MEMORYOP_EA, memOp, IARG_UINT32, INS_MemoryOperandSize(ins, memOp), IARG_ADDRINT, displacement, IARG_END); // Check if the change in memSize above changes anything
    }

    // INS_InsertIfCall(ins, IPOINT_BEFORE, (AFUNPTR) FastForward, IARG_END);
    // INS_InsertThenPredicatedCall(ins, IPOINT_BEFORE, (AFUNPTR) MyPredicatedAnalysis, IARG_UINT32, ins_size, IARG_UINT32, memOperands, IARG_UINT32, readsize, IARG_UINT32, writesize, IARG_UINT32, tempMem, IARG_END);

    // if (memOperands > 0){
    //     INS_InsertIfCall(ins, IPOINT_BEFORE, (AFUNPTR) FastForward, IARG_END);
    //     INS_InsertThenPredicatedCall(ins, IPOINT_BEFORE, (AFUNPTR) MemIncrement, IARG_UINT32, tempRead, IARG_UINT32, tempWrite, IARG_END);
    // }

    // if(category == XED_CATEGORY_NOP)
    // {    
    //     INS_InsertIfCall(ins, IPOINT_BEFORE, (AFUNPTR) FastForward, IARG_END);
    //     INS_InsertThenPredicatedCall(ins, IPOINT_BEFORE, (AFUNPTR) Increment, IARG_UINT32, 2, IARG_END);
    // }

    // else if(category == XED_CATEGORY_CALL)
    // {
    //     if (IsDirect) 
    //     {   
    //         INS_InsertIfCall(ins, IPOINT_BEFORE, (AFUNPTR) FastForward, IARG_END);
    //         INS_InsertThenPredicatedCall(ins, IPOINT_BEFORE, (AFUNPTR) Increment, IARG_UINT32, 3, IARG_END);
    //     }
   
    //     else 
    //     {    
    //         INS_InsertIfCall(ins, IPOINT_BEFORE, (AFUNPTR) FastForward, IARG_END);
    //         INS_InsertThenPredicatedCall(ins, IPOINT_BEFORE, (AFUNPTR) Increment, IARG_UINT32, 4, IARG_END);
    //     }   

    // }

    // else if(category == XED_CATEGORY_RET)
    // {   
    //     INS_InsertIfCall(ins, IPOINT_BEFORE, (AFUNPTR) FastForward, IARG_END);
    //     INS_InsertThenPredicatedCall(ins, IPOINT_BEFORE, (AFUNPTR) Increment, IARG_UINT32, 5, IARG_END);
    // }

    // else if(category == XED_CATEGORY_UNCOND_BR)
    // {   
    //     INS_InsertIfCall(ins, IPOINT_BEFORE, (AFUNPTR) FastForward, IARG_END);
    //     INS_InsertThenPredicatedCall(ins, IPOINT_BEFORE, (AFUNPTR) Increment, IARG_UINT32, 6, IARG_END);
    // }

    // else if(category == XED_CATEGORY_COND_BR)
    // {   
    //     INS_InsertIfCall(ins, IPOINT_BEFORE, (AFUNPTR) FastForward, IARG_END);
    //     INS_InsertThenPredicatedCall(ins, IPOINT_BEFORE, (AFUNPTR) Increment, IARG_UINT32, 7, IARG_END);
    // }

    // else if(category == XED_CATEGORY_LOGICAL)
    // {   
    //     INS_InsertIfCall(ins, IPOINT_BEFORE, (AFUNPTR) FastForward, IARG_END);
    //     INS_InsertThenPredicatedCall(ins, IPOINT_BEFORE, (AFUNPTR) Increment, IARG_UINT32, 8, IARG_END);
    // }

    // else if(category == XED_CATEGORY_ROTATE || category == XED_CATEGORY_SHIFT)
    // {   
    //     INS_InsertIfCall(ins, IPOINT_BEFORE, (AFUNPTR) FastForward, IARG_END);
    //     INS_InsertThenPredicatedCall(ins, IPOINT_BEFORE, (AFUNPTR) Increment, IARG_UINT32, 9, IARG_END);
    // }

    // else if(category == XED_CATEGORY_FLAGOP)
    // {   
    //     INS_InsertIfCall(ins, IPOINT_BEFORE, (AFUNPTR) FastForward, IARG_END);
    //     INS_InsertThenPredicatedCall(ins, IPOINT_BEFORE, (AFUNPTR) Increment, IARG_UINT32, 10, IARG_END);
    // }

    // else if(category == XED_CATEGORY_AVX || category == XED_CATEGORY_AVX2 || category == XED_CATEGORY_AVX2GATHER)
    // {   
    //     INS_InsertIfCall(ins, IPOINT_BEFORE, (AFUNPTR) FastForward, IARG_END);
    //     INS_InsertThenPredicatedCall(ins, IPOINT_BEFORE, (AFUNPTR) Increment, IARG_UINT32, 11, IARG_END);
    // }

    // else if(category == XED_CATEGORY_CMOV)
    // {   
    //     INS_InsertIfCall(ins, IPOINT_BEFORE, (AFUNPTR) FastForward, IARG_END);
    //     INS_InsertThenPredicatedCall(ins, IPOINT_BEFORE, (AFUNPTR) Increment, IARG_UINT32, 12, IARG_END);
    // }

    // else if(category == XED_CATEGORY_MMX || category == XED_CATEGORY_SSE)
    // {   
    //     INS_InsertIfCall(ins, IPOINT_BEFORE, (AFUNPTR) FastForward, IARG_END);
    //     INS_InsertThenPredicatedCall(ins, IPOINT_BEFORE, (AFUNPTR) Increment, IARG_UINT32, 13, IARG_END);
    // }

    // else if(category == XED_CATEGORY_SYSCALL)
    // {   
    //     INS_InsertIfCall(ins, IPOINT_BEFORE, (AFUNPTR) FastForward, IARG_END);
    //     INS_InsertThenPredicatedCall(ins, IPOINT_BEFORE, (AFUNPTR) Increment, IARG_UINT32, 14, IARG_END);
    // }

    // else if(category == XED_CATEGORY_X87_ALU)
    // {   
    //     INS_InsertIfCall(ins, IPOINT_BEFORE, (AFUNPTR) FastForward, IARG_END);
    //     INS_InsertThenPredicatedCall(ins, IPOINT_BEFORE, (AFUNPTR) Increment, IARG_UINT32, 15, IARG_END);
    // }

    // else
    // {   
    //     INS_InsertIfCall(ins, IPOINT_BEFORE, (AFUNPTR) FastForward, IARG_END);
    //     INS_InsertThenPredicatedCall(ins, IPOINT_BEFORE, (AFUNPTR) Increment, IARG_UINT32, 16, IARG_END);
    // }

    INS_InsertIfCall(ins, IPOINT_BEFORE, (AFUNPTR) FastForward, IARG_END);
    INS_InsertThenCall(ins, IPOINT_BEFORE, (AFUNPTR) MyAnalysis, IARG_INST_PTR, IARG_UINT32, ins_size, IARG_UINT32, numOperands, IARG_UINT32, readReg, IARG_UINT32, writeReg, IARG_END);
}

/* ===================================================================== */
// Utilities
/* ===================================================================== */

/*!
 *  Print out help message.
 */
INT32 Usage()
{
    cerr << "This tool prints out the number of dynamically executed " << endl <<
            "instructions, basic blocks and threads in the application." << endl << endl;

    cerr << KNOB_BASE::StringKnobSummary() << endl;

    return -1;
}

/* ===================================================================== */
// Analysis routines
/* ===================================================================== */

/*!
 * Increase counter of the executed basic blocks and instructions.
 * This function is called for every basic block when it is about to be executed.
 * @param[in]   numInstInBbl    number of instructions in the basic block
 * @note use atomic operations for multi-threaded applications
 */
VOID CountBbl(UINT32 numInstInBbl)
{
    bblCount++;
    insCount += numInstInBbl;
    icount += numInstInBbl;
    if (icount >= prev + 1000000000){
        prev = icount - icount%(100000000);
        *out <<  "INS: " << icount << endl;
    }
}

/* ===================================================================== */
// Instrumentation callbacks
/* ===================================================================== */

/*!
 * Insert call to the CountBbl() analysis routine before every basic block 
 * of the trace.
 * This function is called every time a new trace is encountered.
 * @param[in]   trace    trace to be instrumented
 * @param[in]   v        value specified by the tool in the TRACE_AddInstrumentFunction
 *                       function call
 */
VOID Trace(TRACE trace, VOID *v)
{
    // Visit every basic block in the trace
    for (BBL bbl = TRACE_BblHead(trace); BBL_Valid(bbl); bbl = BBL_Next(bbl))
    {
        // Insert a call to CountBbl() before every basic bloc, passing the number of instructions
        BBL_InsertCall(bbl, IPOINT_BEFORE, (AFUNPTR)CountBbl, IARG_UINT32, BBL_NumIns(bbl), IARG_END);
    }
}

/*!
 * Increase counter of threads in the application.
 * This function is called for every thread created by the application when it is
 * about to start running (including the root thread).
 * @param[in]   threadIndex     ID assigned by PIN to the new thread
 * @param[in]   ctxt            initial register state for the new thread
 * @param[in]   flags           thread creation flags (OS specific)
 * @param[in]   v               value specified by the tool in the 
 *                              PIN_AddThreadStartFunction function call
 */
VOID ThreadStart(THREADID threadIndex, CONTEXT *ctxt, INT32 flags, VOID *v)
{
    threadCount++;
}

/*!
 * Print out analysis results.
 * This function is called when the application exits.
 * @param[in]   code            exit code of the application
 * @param[in]   v               value specified by the tool in the 
 *                              PIN_AddFiniFunction function call
 */
VOID Fini(INT32 code, VOID *v)
{
    *out <<  "===============================================" << endl;
    *out <<  "MyPinTool analysis results: " << endl;
    *out <<  "Number of instructions: " << insCount  << endl;
    *out <<  "Number of basic blocks: " << bblCount  << endl;
    *out <<  "Number of threads: " << threadCount  << endl;
    int i;
    for (i=0; i<17; i++) *out <<  "Count: " << INScount[i]  << endl;
    *out <<  "===============================================" << endl;
}

/*!
 * The main procedure of the tool.
 * This function is called when the application image is loaded but not yet started.
 * @param[in]   argc            total number of elements in the argv array
 * @param[in]   argv            array of command line arguments, 
 *                              including pin -t <toolname> -- ...
 */
int main(int argc, char *argv[])
{
    // Initialize PIN library. Print help message if -h(elp) is specified
    // in the command line or the command line is invalid 
    if( PIN_Init(argc,argv) )
    {
        return Usage();
    }
    
    fast_forward_count = FastForwardCount.Value()*1000000000;

    string fileName = KnobOutputFile.Value();

    if (!fileName.empty()) { out = new std::ofstream(fileName.c_str());}

    if (KnobCount)
    {
        // Register function to be called to instrument traces
        // TRACE_AddInstrumentFunction(Trace, 0);

        // Register function to be called for every thread before it starts running
        PIN_AddThreadStartFunction(ThreadStart, 0);

        INS_AddInstrumentFunction(Instruction, 0);

        // Register function to be called when the application exits
        PIN_AddFiniFunction(Fini, 0);
    }
    
    cerr <<  "===============================================" << endl;
    cerr <<  "This application is instrumented by MyPinTool" << endl;
    if (!KnobOutputFile.Value().empty()) 
    {
        cerr << "See file " << KnobOutputFile.Value() << " for analysis results" << endl;
    }
    cerr <<  "===============================================" << endl;

    // Start the program, never returns
    PIN_StartProgram();
    
    return 0;
}

/* ===================================================================== */
/* eof */
/* ===================================================================== */
