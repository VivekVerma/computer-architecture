#include "pin.H"

using namespace std;

// Note:
//
// PC should be in UINT32 data type
// targetPC should be in UINT32 data type

class FNBT
{
  public:

    UINT32 count[2][2];

    FNBT()
    {
      count[0][0] = 0;
      count[1][0] = 0;
      count[0][1] = 0;
      count[1][1] = 0;
    };

    bool predict(UINT32 PC, UINT32 targetPC)
    {
       if (targetPC < PC) return true;
       return false;
    }

    void update(UINT32 PC, bool pred, bool taken){
        count[(int)pred][(int)taken]++;
    }

    void print(ostream*& out)
    {
      *out << "FNBT" << endl;
      *out << "Pred\\Actual\tNT\tT" << endl;
      *out << "NT\t" << count[0][0] << "\t" << count[0][1] << endl;
      *out << "T \t" << count[1][0] << "\t" << count[1][1] << endl;
      *out << "Accuracy: " << (1.0*(count[0][0]+count[1][1]))/(count[0][0]+count[1][1]+count[1][0]+count[0][1]) << endl;
      *out << "Forward Mispredictions: " << (1.0*count[0][1])/(count[0][0]+count[0][1]) << endl;
      *out << "Backward Mispredictions: " << (1.0*count[1][0])/(count[1][0]+count[1][1]) << endl << endl;
    }
};

class BiModal
{
  private:

    INT32 PHT[512];
    UINT32 fcount[2];
    UINT32 bcount[2];
    UINT32 count[2][2];

  public:

    BiModal()
    {
      count[0][0] = 0;
      count[1][0] = 0;
      count[0][1] = 0;
      count[1][1] = 0;

      fcount[0] = 0;
      fcount[1] = 0;
      bcount[0] = 0;
      bcount[1] = 0;

      for(UINT32 i=0; i<512; i++)
      {
        PHT[i] = 0;
      }
    }

    bool predict(UINT32 PC)
    {
      if (PHT[PC%512] <= 0) return false;
      return true;
    }

    void update(UINT32 PC, bool pred, bool taken, bool forward)
    {
      if (taken==true && PHT[PC%512] < 2)
      {
        PHT[PC%512]++;
      }
      else if (taken==false && PHT[PC%512] > -1)
      {
        PHT[PC%512]--;
      }

      count[(int)pred][(int)taken]++;

      if (forward && taken==pred) fcount[1]++;
      else if (forward && taken!=pred) fcount[0]++;
      else if (!forward && taken==pred) bcount[1]++;
      else bcount[0]++;

    }

    void print(ostream*& out)
    {
      *out << "BiModal" << endl;
      *out << "Pred\\Actual\tNT\tT" << endl;
      *out << "NT\t" << count[0][0] << "\t" << count[0][1] << endl;
      *out << "T \t" << count[1][0] << "\t" << count[1][1] << endl;
      *out << "Accuracy: " << (1.0*(count[0][0]+count[1][1]))/(count[0][0]+count[1][1]+count[1][0]+count[0][1]) << endl;
      *out << "Forward Mispredictions: " << (1.0*fcount[0])/(fcount[0]+fcount[1]) << endl;
      *out << "Backward Mispredictions: " << (1.0*bcount[0])/(bcount[0]+bcount[1]) << endl << endl;
    }
};

class SAg
{
  private:

    UINT32 BHT[1024];
    INT32 PHT[512];
    UINT32 fcount[2];
    UINT32 bcount[2];
    UINT32 count[2][2];

  public:

    SAg()
    {
      count[0][0] = 0;
      count[1][0] = 0;
      count[0][1] = 0;
      count[1][1] = 0;

      fcount[0] = 0;
      fcount[1] = 0;
      bcount[0] = 0;
      bcount[1] = 0;

      for(UINT32 i=0; i<1024; i++)
      {
        BHT[i] = 0;
      }

      for(UINT32 i=0; i<512; i++)
      {
        PHT[i] = 0;
      }
    }

    bool predict(UINT32 PC)
    {
      if (PHT[BHT[PC%1024]] <= 0) return false;
      return true;
    }

    void update(UINT32 PC, bool pred, bool taken, bool forward)
    {
      if (taken==true && PHT[BHT[PC%1024]] < 2)
      {
        PHT[BHT[PC%1024]]++;
      }
      else if (taken==false && PHT[BHT[PC%1024]] > -1)
      {
        PHT[BHT[PC%1024]]--;
      }

      if (taken==true) BHT[PC%1024] = (2*BHT[PC%1024] + 1)%512;
      else BHT[PC%1024] = (2*BHT[PC%1024])%512;

      count[(int)pred][(int)taken]++;

      if (forward && taken==pred) fcount[1]++;
      else if (forward && taken!=pred) fcount[0]++;
      else if (!forward && taken==pred) bcount[1]++;
      else bcount[0]++;
    }

    void print(ostream*& out)
    {
      *out << "SAg" << endl;
      *out << "Pred\\Actual\tNT\tT" << endl;
      *out << "NT\t" << count[0][0] << "\t" << count[0][1] << endl;
      *out << "T \t" << count[1][0] << "\t" << count[1][1] << endl;
      *out << "Accuracy: " << (1.0*(count[0][0]+count[1][1]))/(count[0][0]+count[1][1]+count[1][0]+count[0][1]) << endl;
      *out << "Forward Mispredictions: " << (1.0*fcount[0])/(fcount[0]+fcount[1]) << endl;
      *out << "Backward Mispredictions: " << (1.0*bcount[0])/(bcount[0]+bcount[1]) << endl << endl;
    }
};

class GAg
{
  private:

    INT32 PHT[512];
    UINT32 fcount[2];
    UINT32 bcount[2];
    UINT32 count[2][2];

  public:

    UINT32 BHT;

    GAg()
    {
      count[0][0] = 0;
      count[1][0] = 0;
      count[0][1] = 0;
      count[1][1] = 0;

      fcount[0] = 0;
      fcount[1] = 0;
      bcount[0] = 0;
      bcount[1] = 0;

      BHT = 0;

      for(UINT32 i=0; i<512; i++)
      {
        PHT[i] = 0;
      }
    }

    bool predict()
    {
      if (PHT[BHT] <= 0) return false;
      return true;
    }

    void update(bool pred, bool taken, bool forward)
    {
      if (taken==true && PHT[BHT] < 4)
      {
        PHT[BHT]++;
      }
      else if (taken==false && PHT[BHT] > -3)
      {
        PHT[BHT]--;
      }

      if (taken==true) BHT = (2*BHT + 1)%512;
      else BHT = (2*BHT)%512;

      count[(int)pred][(int)taken]++;

      if (forward && taken==pred) fcount[1]++;
      else if (forward && taken!=pred) fcount[0]++;
      else if (!forward && taken==pred) bcount[1]++;
      else bcount[0]++;
    }

    void print(ostream*& out)
    {
      *out << "GAg" << endl;
      *out << "Pred\\Actual\tNT\tT" << endl;
      *out << "NT\t" << count[0][0] << "\t" << count[0][1] << endl;
      *out << "T \t" << count[1][0] << "\t" << count[1][1] << endl;
      *out << "Accuracy: " << (1.0*(count[0][0]+count[1][1]))/(count[0][0]+count[1][1]+count[1][0]+count[0][1]) << endl;
      *out << "Forward Mispredictions: " << (1.0*fcount[0])/(fcount[0]+fcount[1]) << endl;
      *out << "Backward Mispredictions: " << (1.0*bcount[0])/(bcount[0]+bcount[1]) << endl << endl;
    }
};

class gShare
{
private:

  UINT32 BHT;
  INT32 PHT[512];
  UINT32 fcount[2];
  UINT32 bcount[2];
  UINT32 count[2][2];

public:

  gShare()
  {
    count[0][0] = 0;
    count[1][0] = 0;
    count[0][1] = 0;
    count[1][1] = 0;

    fcount[0] = 0;
    fcount[1] = 0;
    bcount[0] = 0;
    bcount[1] = 0;

    BHT = 0;

    for(UINT32 i=0; i<512; i++)
    {
      PHT[i] = 0;
    }
  }

  bool predict(UINT32 PC)
  {
    if (PHT[((UINT32)(BHT^PC))%512] <= 0) return false;
    return true;
  }

  void update(UINT32 PC, bool pred, bool taken, bool forward)
  {
    if (taken==true && PHT[((UINT32)(BHT^PC))%512] < 4)
    {
      PHT[((UINT32)(BHT^PC))%512]++;
    }
    else if (taken==false && PHT[((UINT32)(BHT^PC))%512] > -3)
    {
      PHT[((UINT32)(BHT^PC))%512]--;
    }

    if (taken==true) BHT = (2*BHT + 1)%512;
    else BHT = (2*BHT)%512;

    count[(int)pred][(int)taken]++;

    if (forward && taken==pred) fcount[1]++;
    else if (forward && taken!=pred) fcount[0]++;
    else if (!forward && taken==pred) bcount[1]++;
    else bcount[0]++;
  }

  void print(ostream*& out)
  {
    *out << "gShare" << endl;
    *out << "Pred\\Actual\tNT\tT" << endl;
    *out << "NT\t" << count[0][0] << "\t" << count[0][1] << endl;
    *out << "T \t" << count[1][0] << "\t" << count[1][1] << endl;
    *out << "Accuracy: " << (1.0*(count[0][0]+count[1][1]))/(count[0][0]+count[1][1]+count[1][0]+count[0][1]) << endl;
    *out << "Forward Mispredictions: " << (1.0*fcount[0])/(fcount[0]+fcount[1]) << endl;
    *out << "Backward Mispredictions: " << (1.0*bcount[0])/(bcount[0]+bcount[1]) << endl << endl;
  }

};

class Hybrid1
{
  private:

    SAg _SAg;
    GAg _GAg;
    INT32 Meta[512];
    bool taken1;
    bool taken2;
    UINT32 fcount[2];
    UINT32 bcount[2];
    UINT32 count[2][2];

  public:

    Hybrid1()
    {
      count[0][0] = 0;
      count[1][0] = 0;
      count[0][1] = 0;
      count[1][1] = 0;

      fcount[0] = 0;
      fcount[1] = 0;
      bcount[0] = 0;
      bcount[1] = 0;

      for(UINT32 i=0; i<512; i++)
      {
        Meta[i] = 0;
      }
    }

    bool predict(UINT32 PC)
    {
      taken1 = _SAg.predict(PC);
      taken2 = _GAg.predict();

      if (Meta[_GAg.BHT] <= 0) return taken1;
      return taken2;
    }

    void update(UINT32 PC, bool pred, bool taken, bool forward){
      if (taken1 != taken2)
      {
        if (taken==taken1 && Meta[_GAg.BHT] > -1) Meta[_GAg.BHT]--;
        else if (taken==taken2 && Meta[_GAg.BHT] < 2) Meta[_GAg.BHT]++;
      }

      _SAg.update(PC,taken1,taken,forward);
      _GAg.update(taken2,taken,forward);

      count[(int)pred][(int)taken]++;

      if (forward && taken==pred) fcount[1]++;
      else if (forward && taken!=pred) fcount[0]++;
      else if (!forward && taken==pred) bcount[1]++;
      else bcount[0]++;
    }

    void print(ostream*& out)
    {
      *out << "Hybrid - SAg & GAg" << endl;
      *out << "Pred\\Actual\tNT\tT" << endl;
      *out << "NT\t" << count[0][0] << "\t" << count[0][1] << endl;
      *out << "T \t" << count[1][0] << "\t" << count[1][1] << endl;
      *out << "Accuracy: " << (1.0*(count[0][0]+count[1][1]))/(count[0][0]+count[1][1]+count[1][0]+count[0][1]) << endl;
      *out << "Forward Mispredictions: " << (1.0*fcount[0])/(fcount[0]+fcount[1]) << endl;
      *out << "Backward Mispredictions: " << (1.0*bcount[0])/(bcount[0]+bcount[1]) << endl << endl;
    }
};

class Hybrid2_1
{
  private:

    SAg _SAg;
    GAg _GAg;
    gShare _gShare;
    bool taken1;
    bool taken2;
    bool taken3;
    UINT32 fcount[2];
    UINT32 bcount[2];
    UINT32 count[2][2];

  public:

    Hybrid2_1()
    {
      count[0][0] = 0;
      count[1][0] = 0;
      count[0][1] = 0;
      count[1][1] = 0;

      fcount[0] = 0;
      fcount[1] = 0;
      bcount[0] = 0;
      bcount[1] = 0;
    }

    bool predict(UINT32 PC)
    {
      taken1 = _SAg.predict(PC);
      taken2 = _GAg.predict();
      taken3 = _gShare.predict(PC);

      if (taken1==taken2) return taken1;
      else if (taken1==taken3) return taken1;
      return taken2;
    }

    void update(UINT32 PC, bool pred, bool taken, bool forward){

      _SAg.update(PC,taken1,taken,forward);
      _GAg.update(taken2,taken,forward);
      _gShare.update(PC,taken3,taken,forward);

      count[(int)pred][(int)taken]++;

      if (forward && taken==pred) fcount[1]++;
      else if (forward && taken!=pred) fcount[0]++;
      else if (!forward && taken==pred) bcount[1]++;
      else bcount[0]++;
    }

    void print(ostream*& out)
    {
      *out << "Hybrid - SAg & GAg & gShare - Majority" << endl;
      *out << "Pred\\Actual\tNT\tT" << endl;
      *out << "NT\t" << count[0][0] << "\t" << count[0][1] << endl;
      *out << "T \t" << count[1][0] << "\t" << count[1][1] << endl;
      *out << "Accuracy: " << (1.0*(count[0][0]+count[1][1]))/(count[0][0]+count[1][1]+count[1][0]+count[0][1]) << endl;
      *out << "Forward Mispredictions: " << (1.0*fcount[0])/(fcount[0]+fcount[1]) << endl;
      *out << "Backward Mispredictions: " << (1.0*bcount[0])/(bcount[0]+bcount[1]) << endl << endl;
    }
};

class Hybrid2_2
{
  private:

    SAg _SAg;
    GAg _GAg;
    gShare _gShare;
    INT32 Meta[3][512];
    bool taken1;
    bool taken2;
    bool taken3;
    bool pick1; // Between SAg and GAg
    bool pick2; // Between GAg and gShare
    bool pick3; // Between gShare and SAg
    UINT32 fcount[2];
    UINT32 bcount[2];
    UINT32 count[2][2];

  public:

    Hybrid2_2()
    {
      count[0][0] = 0;
      count[1][0] = 0;
      count[0][1] = 0;
      count[1][1] = 0;

      fcount[0] = 0;
      fcount[1] = 0;
      bcount[0] = 0;
      bcount[1] = 0;

      for(UINT32 i=0; i<512; i++)
      {
        Meta[0][i] = 0;
        Meta[1][i] = 0;
        Meta[2][i] = 0;
      }
    }

    bool predict(UINT32 PC)
    {
      taken1 = _SAg.predict(PC);
      taken2 = _GAg.predict();
      taken3 = _gShare.predict(PC);

      if (Meta[0][_GAg.BHT] <= 0) pick1 = taken1;
      else pick1 = taken2;
      if (Meta[1][_GAg.BHT] <= 0) pick2 = taken2;
      else pick2 = taken3;
      if (Meta[2][_GAg.BHT] <= 0) pick3 = taken3;
      else pick3 = taken1;

      if (pick1==pick2) return pick1;
      else if (pick1==pick3) return pick1;
      return pick2;
    }

    void update(UINT32 PC, bool pred, bool taken, bool forward){
      if (taken1 != taken2)
      {
        if (taken==taken1 && Meta[0][_GAg.BHT] > -1) Meta[0][_GAg.BHT]--;
        else if (taken==taken2 && Meta[0][_GAg.BHT] < 2) Meta[0][_GAg.BHT]++;
      }

      if (taken2 != taken3)
      {
        if (taken==taken2 && Meta[1][_GAg.BHT] > -1) Meta[1][_GAg.BHT]--;
        else if (taken==taken3 && Meta[1][_GAg.BHT] < 2) Meta[1][_GAg.BHT]++;
      }

      if (taken3 != taken1)
      {
        if (taken==taken3 && Meta[2][_GAg.BHT] > -1) Meta[2][_GAg.BHT]--;
        else if (taken==taken1 && Meta[2][_GAg.BHT] < 2) Meta[2][_GAg.BHT]++;
      }

      _SAg.update(PC,taken1,taken,forward);
      _GAg.update(taken2,taken,forward);
      _gShare.update(PC,taken3,taken,forward);

      count[(int)pred][(int)taken]++;

      if (forward && taken==pred) fcount[1]++;
      else if (forward && taken!=pred) fcount[0]++;
      else if (!forward && taken==pred) bcount[1]++;
      else bcount[0]++;
    }

    void print(ostream*& out)
    {
      *out << "Hybrid - SAg & GAg & gShare - Meta Predictors" << endl;
      *out << "Pred\\Actual\tNT\tT" << endl;
      *out << "NT\t" << count[0][0] << "\t" << count[0][1] << endl;
      *out << "T \t" << count[1][0] << "\t" << count[1][1] << endl;
      *out << "Accuracy: " << (1.0*(count[0][0]+count[1][1]))/(count[0][0]+count[1][1]+count[1][0]+count[0][1]) << endl;
      *out << "Forward Mispredictions: " << (1.0*fcount[0])/(fcount[0]+fcount[1]) << endl;
      *out << "Backward Mispredictions: " << (1.0*bcount[0])/(bcount[0]+bcount[1]) << endl << endl;
    }
};

class BTB1 // return 0 if miss else return targetPC
{

  private:

    bool valid[128][4];
    UINT32 tag[128][4];
    UINT32 target[128][4];
    UINT32 timestamp[128][4];
    UINT32 time_counter;
    UINT32 miss;
    UINT32 hit;

  public:

    UINT32 count[2];

    BTB1()
    {
      for (UINT32 i=0; i<128; i++)
      {
        for (UINT32 j=0; j<4; j++)
        {
          valid[i][j] = false;
          timestamp[i][j] = 0;
        }
      }
      time_counter = 0;
      miss = 0;
      hit = 0;
    }

    UINT32 predict(UINT32 PC)
    {
      for (UINT32 i=0; i<4; i++)
      {
        if (valid[PC%128][i] && PC==tag[PC%128][i])
        {
          timestamp[PC%128][i] = time_counter;
          time_counter++;
          return target[PC%128][i];
        }
      }
      return 0;
    }

    void update(UINT32 PC, UINT32 predPC, UINT32 targetPC, bool taken)
    {
      if (predPC==0) // Miss and Add to BTB
      {
        bool check = true;
        for (UINT32 i=0; i<4; i++)
        {
          if (!valid[PC%128][i]) // Replace any invalid entry
          {
            valid[PC%128][i] = true;
            tag[PC%128][i] = PC;
            target[PC%128][i] = targetPC;
            check = false;
            break;
          }
        }
        if (check) // LRU Replacement
        {
          UINT32 lru = 0;
          for (UINT32 i=0; i<4; i++)
          {
            if (timestamp[PC%128][i] < timestamp[PC%128][lru]) lru = i;
          }
          tag[PC%128][lru] = PC;
          target[PC%128][lru] = targetPC;
        }
        miss++;
        if (taken) count[0]++;
        else count[1]++;
      }

      else if (predPC != targetPC) // Hit and Misprediction
      {
        if (taken)
        {
          for (UINT32 i=0; i<4; i++)
          {
            if (valid[PC%128][i] && PC==tag[PC%128][i])
            {
              target[PC%128][i] = targetPC;
              break;
            }
          }
        }
        else
        {
          for (UINT32 i=0; i<4; i++)
          {
            if (valid[PC%128][i] && PC==tag[PC%128][i])
            {
              valid[PC%128][i] = false;
              break;
            }
          }
        }
        hit++;
        count[0]++;
      }

      else
      {
        hit++;
        count[1]++;
      }
    }

    void print(ostream*& out)
    {
      *out << "BTB indexed with PC" << endl;
      *out << "Correct Predictions:\t" << count[1] << ", Mispredictions:\t" << count[0] << endl;
      *out << "Hit:\t" << hit << ", Miss:\t" << miss << endl;
      *out << "Accuracy:\t" << (1.0*count[1])/(count[0]+count[1]) << ", Misprediction rate:\t" << (1.0*count[0])/(count[0]+count[1]) << endl;
      *out << "Hit rate:\t" << (1.0*hit)/(hit+miss) << ", Miss rate:\t" << (1.0*miss)/(hit+miss) << endl << endl;
    }
};

class BTB2 // return 0 if miss else return targetPC
{

  private:

    bool valid[128][4];
    UINT32 tag[128][4];
    UINT32 target[128][4];
    UINT32 timestamp[128][4];
    UINT32 time_counter;
    UINT32 miss;
    UINT32 hit;

  public:

    UINT32 count[2];

    BTB2()
    {
      for (UINT32 i=0; i<128; i++)
      {
        for (UINT32 j=0; j<4; j++)
        {
          valid[i][j] = false;
          timestamp[i][j] = 0;
        }
      }
      time_counter = 0;
      miss = 0;
      hit = 0;
    }

    UINT32 predict(UINT32 PC, UINT32 GHP)
    {
      for (UINT32 i=0; i<4; i++)
      {
        if (valid[((UINT32)(GHP^PC))%128][i] && PC==tag[((UINT32)(GHP^PC))%128][i])
        {
          timestamp[((UINT32)(GHP^PC))%128][i] = time_counter;
          time_counter++;
          return target[((UINT32)(GHP^PC))%128][i];
        }
      }
      return 0;
    }

    void update(UINT32 PC, UINT32 predPC, UINT32 targetPC, UINT32 GHP, bool taken)
    {
      if (predPC==0) // Miss and Add to BTB
      {
        bool check = true;
        for (UINT32 i=0; i<4; i++)
        {
          if (!valid[((UINT32)(GHP^PC))%128][i]) // Replace any invalid entry
          {
            valid[((UINT32)(GHP^PC))%128][i] = true;
            tag[((UINT32)(GHP^PC))%128][i] = PC;
            target[((UINT32)(GHP^PC))%128][i] = targetPC;
            check = false;
            break;
          }
        }
        if (check) // LRU Replacement
        {
          UINT32 lru = 0;
          for (UINT32 i=0; i<4; i++)
          {
            if (timestamp[((UINT32)(GHP^PC))%128][i] < timestamp[((UINT32)(GHP^PC))%128][lru]) lru = i;
          }
          tag[((UINT32)(GHP^PC))%128][lru] = PC;
          target[((UINT32)(GHP^PC))%128][lru] = targetPC;
        }
        miss++;
        if (taken) count[0]++;
        else count[1]++;
      }

      else if (predPC != targetPC) // Hit and Misprediction
      {
        if (taken)
        {
          for (UINT32 i=0; i<4; i++)
          {
            if (valid[((UINT32)(GHP^PC))%128][i] && PC==tag[((UINT32)(GHP^PC))%128][i])
            {
              target[((UINT32)(GHP^PC))%128][i] = targetPC;
              break;
            }
          }
        }
        else
        {
          for (UINT32 i=0; i<4; i++)
          {
            if (valid[((UINT32)(GHP^PC))%128][i] && PC==tag[((UINT32)(GHP^PC))%128][i])
            {
              valid[((UINT32)(GHP^PC))%128][i] = false;
              break;
            }
          }
        }
        hit++;
        count[0]++;
      }

      else
      {
        hit++;
        count[1]++;
      }
    }

    void print(ostream*& out)
    {
      *out << "BTB indexed with a hash of PC and global path history" << endl;
      *out << "Correct Predictions:\t" << count[1] << ", Mispredictions:\t" << count[0] << endl;
      *out << "Hit:\t" << hit << ", Miss:\t" << miss << endl;
      *out << "Accuracy:\t" << (1.0*count[1])/(count[0]+count[1]) << ", Misprediction rate:\t" << (1.0*count[0])/(count[0]+count[1]) << endl;
      *out << "Hit rate:\t" << (1.0*hit)/(hit+miss) << ", Miss rate:\t" << (1.0*miss)/(hit+miss) << endl << endl;
    }
};
