#include "pin.H"
#include <math.h> 

using namespace std;

// Note:
//
// PC should be in UINT32 data type
// targetPC should be in UINT32 data type

#define INDEX 128
#define INDEX_old 512
#define INDEX_ada 24
#define HISTORY_LENGTH 32
#define HISTORY_LENGTH_old 16
#define HISTORY_LENGTH_ada 8

UINT32 _pow(UINT32 a, UINT32 b){
  if (b==0) return 1;
  if (b==1) return a;
  UINT32 temp = _pow(a,b/2);
  if (b%2==0) return temp*temp;
  else return temp*temp*a;
}

UINT32 _abs(INT32 a){
  if (a<0) return -a;
  return a;
}

INT32 _max(INT32 a, INT32 b){
  if (a>b) return a;
  return b;
}

INT32 _min(INT32 a, INT32 b){
  if (a<b) return a;
  return b;
}

class Perceptron_old
{
  private:

    INT32 perceptron[INDEX_old][HISTORY_LENGTH_old];
    UINT32 fcount[2];
    UINT32 bcount[2];
    UINT32 count[2][2];
    UINT32 threshold;
    INT32 GHR[HISTORY_LENGTH_old];
    UINT32 GHR_index;
    INT32 last_result;
    UINT32 weight_threshold;

  public: 

    Perceptron_old()
    {

      threshold = log2(1.93*HISTORY_LENGTH_old + 14);
      threshold = 1;

      weight_threshold = _pow(2,threshold);

      last_result = 0.0;

      GHR_index = 0;

      count[0][0] = 0;
      count[1][0] = 0;
      count[0][1] = 0;
      count[1][1] = 0;

      fcount[0] = 0;
      fcount[1] = 0;
      bcount[0] = 0;
      bcount[1] = 0;

      for(UINT32 i=0; i<HISTORY_LENGTH_old; i++)
        {
        GHR[i] = 1-2*(i%2);
        }

      for(UINT32 i=0; i<INDEX_old; i++)
      {
        for(UINT32 j=0; j<HISTORY_LENGTH_old; j++)
        {
        perceptron[i][j] = 0;
        }
      }
    }
  
    bool predict(UINT32 PC)
    {
      INT32 result = 0;
      for (UINT32 i=0; i<HISTORY_LENGTH_old; i++){
        result += perceptron[PC%INDEX_old][i]*GHR[(i+GHR_index)%HISTORY_LENGTH_old];
      }
      last_result = result;
      if (result > 0){
        return TRUE;
      }
      return FALSE;
    }

    void update(UINT32 PC, bool pred, bool taken, bool forward)
    {
      if (taken != pred || _abs(last_result) < threshold)
      {
        if (taken){
          for (UINT32 i=0; i<HISTORY_LENGTH_old; i++){
            perceptron[PC%INDEX_old][i] = _min(perceptron[PC%INDEX_old][i] + GHR[(i+GHR_index)%HISTORY_LENGTH_old], weight_threshold);
          }
      }
        else{
          for (UINT32 i=0; i<HISTORY_LENGTH_old; i++){
            perceptron[PC%INDEX_old][i] = _max(perceptron[PC%INDEX_old][i] - GHR[(i+GHR_index)%HISTORY_LENGTH_old], -weight_threshold);
          }
        }
      }
      
      if (taken==true) GHR[GHR_index%HISTORY_LENGTH_old] = 1;
      else  GHR[GHR_index%HISTORY_LENGTH_old] = -1;
      GHR_index++;

      count[(int)pred][(int)taken]++;

      if (forward && taken==pred) fcount[1]++;
      else if (forward && taken!=pred) fcount[0]++;
      else if (!forward && taken==pred) bcount[1]++;
      else bcount[0]++;
    }

    void print(ostream*& out)
    {
      *out << "Perceptron_old" << endl;
      *out << "INDEX = " << INDEX_old << ", HISTORY_LENGTH = " << HISTORY_LENGTH_old << ", weight_threshold = " << weight_threshold << endl;
      *out << "Pred\\Actual\tNT\tT" << endl;
      *out << "NT\t" << count[0][0] << "\t" << count[0][1] << endl;
      *out << "T \t" << count[1][0] << "\t" << count[1][1] << endl;
      *out << "Accuracy: " << (1.0*(count[0][0]+count[1][1]))/(count[0][0]+count[1][1]+count[1][0]+count[0][1]) << endl;
      *out << "Forward Mispredictions: " << (1.0*fcount[0])/(fcount[0]+fcount[1]) << endl;
      *out << "Backward Mispredictions: " << (1.0*bcount[0])/(bcount[0]+bcount[1]) << endl << endl;
    }
};

class Perceptron
{
  private:

    INT32 perceptron[INDEX][HISTORY_LENGTH];
    UINT32 fcount[2];
    UINT32 bcount[2];
    UINT32 count[2][2];
    UINT32 threshold;
    INT32 GHR[HISTORY_LENGTH];
    UINT32 GHR_index;
    INT32 last_result;
    UINT32 weight_threshold;

  public: 

    Perceptron()
    {

      threshold = log2(1.93*HISTORY_LENGTH + 14);
      // threshold = 1;

      weight_threshold = _pow(2,threshold);

      last_result = 0.0;

      GHR_index = 0;

      count[0][0] = 0;
      count[1][0] = 0;
      count[0][1] = 0;
      count[1][1] = 0;

      fcount[0] = 0;
      fcount[1] = 0;
      bcount[0] = 0;
      bcount[1] = 0;

      for(UINT32 i=0; i<HISTORY_LENGTH; i++)
        {
        GHR[i] = 1-2*(i%2);
        }

      for(UINT32 i=0; i<INDEX; i++)
      {
        for(UINT32 j=0; j<HISTORY_LENGTH; j++)
        {
        perceptron[i][j] = 0;
        }
      }
    }
  
    bool predict(UINT32 PC)
    {
      INT32 result = 0;
      for (UINT32 i=0; i<HISTORY_LENGTH; i++){
        result += perceptron[PC%INDEX][i]*GHR[(i+GHR_index)%HISTORY_LENGTH];
      }
      last_result = result;
      if (result > 0){
        return TRUE;
      }
      return FALSE;
    }

    void update(UINT32 PC, bool pred, bool taken, bool forward)
    {
      if (taken != pred)
      {
        if (taken){
          for (UINT32 i=0; i<HISTORY_LENGTH; i++){
            perceptron[PC%INDEX][i] = _min(perceptron[PC%INDEX][i] + GHR[(i+GHR_index)%HISTORY_LENGTH], weight_threshold);
          }
      }
        else{
          for (UINT32 i=0; i<HISTORY_LENGTH; i++){
            perceptron[PC%INDEX][i] = _max(perceptron[PC%INDEX][i] - GHR[(i+GHR_index)%HISTORY_LENGTH], -weight_threshold);
          }
        }
      }
      
      if (taken==true) GHR[GHR_index%HISTORY_LENGTH] = 1;
      else  GHR[GHR_index%HISTORY_LENGTH] = -1;
      GHR_index++;

      count[(int)pred][(int)taken]++;

      if (forward && taken==pred) fcount[1]++;
      else if (forward && taken!=pred) fcount[0]++;
      else if (!forward && taken==pred) bcount[1]++;
      else bcount[0]++;
    }

    void print(ostream*& out)
    {
      *out << "Perceptron" << endl;
      *out << "INDEX = " << INDEX << ", HISTORY_LENGTH = " << HISTORY_LENGTH << ", weight_threshold = " << weight_threshold << endl;
      *out << "Pred\\Actual\tNT\tT" << endl;
      *out << "NT\t" << count[0][0] << "\t" << count[0][1] << endl;
      *out << "T \t" << count[1][0] << "\t" << count[1][1] << endl;
      *out << "Accuracy: " << (1.0*(count[0][0]+count[1][1]))/(count[0][0]+count[1][1]+count[1][0]+count[0][1]) << endl;
      *out << "Forward Mispredictions: " << (1.0*fcount[0])/(fcount[0]+fcount[1]) << endl;
      *out << "Backward Mispredictions: " << (1.0*bcount[0])/(bcount[0]+bcount[1]) << endl << endl;
    }
};


class gShare
{
private:

  INT32 PHT[32][256];
  UINT32 fcount[2];
  UINT32 bcount[2];
  UINT32 count[2][2];

public:

  UINT32 BHT;
  
  gShare()
  {
    count[0][0] = 0;
    count[1][0] = 0;
    count[0][1] = 0;
    count[1][1] = 0;

    fcount[0] = 0;
    fcount[1] = 0;
    bcount[0] = 0;
    bcount[1] = 0;

    BHT = 0;

    for(UINT32 i=0; i<32; i++)
    {
      for (UINT32 j=0; j<256; j++){
        PHT[i][j] = 0;
      }
    }
  }

  bool predict(UINT32 PC)
  {
    if (PHT[PC%32][((UINT32)(BHT^PC))%256] <= 0) return false;
    return true;
  }

  void update(UINT32 PC, bool pred, bool taken, bool forward)
  {
    if (taken==true && PHT[PC%32][((UINT32)(BHT^PC))%256] < 8)
    {
      PHT[PC%32][((UINT32)(BHT^PC))%256]++;
    }
    else if (taken==false && PHT[PC%32][((UINT32)(BHT^PC))%256] > -7)
    {
      PHT[PC%32][((UINT32)(BHT^PC))%256]--;
    }

    if (taken==true) BHT = (2*BHT + 1)%256;
    else BHT = (2*BHT)%256;

    count[(int)pred][(int)taken]++;

    if (forward && taken==pred) fcount[1]++;
    else if (forward && taken!=pred) fcount[0]++;
    else if (!forward && taken==pred) bcount[1]++;
    else bcount[0]++;
  }

  void print(ostream*& out)
  {
    *out << "gShare" << endl;
    *out << "Pred\\Actual\tNT\tT" << endl;
    *out << "NT\t" << count[0][0] << "\t" << count[0][1] << endl;
    *out << "T \t" << count[1][0] << "\t" << count[1][1] << endl;
    *out << "Accuracy: " << (1.0*(count[0][0]+count[1][1]))/(count[0][0]+count[1][1]+count[1][0]+count[0][1]) << endl;
    *out << "Forward Mispredictions: " << (1.0*fcount[0])/(fcount[0]+fcount[1]) << endl;
    *out << "Backward Mispredictions: " << (1.0*bcount[0])/(bcount[0]+bcount[1]) << endl << endl;
  }

};

class Hybrid
{
  private:

    Perceptron _Perceptron;
    gShare _gShare;
    INT32 Meta[256];
    bool taken1;
    bool taken2;
    UINT32 fcount[2];
    UINT32 bcount[2];
    UINT32 count[2][2];

  public:

    Hybrid()
    {
      count[0][0] = 0;
      count[1][0] = 0;
      count[0][1] = 0;
      count[1][1] = 0;

      fcount[0] = 0;
      fcount[1] = 0;
      bcount[0] = 0;
      bcount[1] = 0;

      for(UINT32 i=0; i<256; i++)
      {
        Meta[i] = 0;
      }
    }

    bool predict(UINT32 PC)
    {
      taken1 = _Perceptron.predict(PC);
      taken2 = _gShare.predict(PC);

      if (Meta[_gShare.BHT] <= 0) return taken1;
      return taken2;
    }

    void update(UINT32 PC, bool pred, bool taken, bool forward){
      if (taken1 != taken2)
      {
        if (taken==taken1 && Meta[_gShare.BHT] > -1) Meta[_gShare.BHT]--;
        else if (taken==taken2 && Meta[_gShare.BHT] < 2) Meta[_gShare.BHT]++;
      }

      _Perceptron.update(PC,taken1,taken,forward);
      _gShare.update(PC,taken2,taken,forward);

      count[(int)pred][(int)taken]++;

      if (forward && taken==pred) fcount[1]++;
      else if (forward && taken!=pred) fcount[0]++;
      else if (!forward && taken==pred) bcount[1]++;
      else bcount[0]++;
    }

    void print(ostream*& out)
    {
      *out << "Hybrid - Perceptron & gShare" << endl;
      *out << "Pred\\Actual\tNT\tT" << endl;
      *out << "NT\t" << count[0][0] << "\t" << count[0][1] << endl;
      *out << "T \t" << count[1][0] << "\t" << count[1][1] << endl;
      *out << "Accuracy: " << (1.0*(count[0][0]+count[1][1]))/(count[0][0]+count[1][1]+count[1][0]+count[0][1]) << endl;
      *out << "Forward Mispredictions: " << (1.0*fcount[0])/(fcount[0]+fcount[1]) << endl;
      *out << "Backward Mispredictions: " << (1.0*bcount[0])/(bcount[0]+bcount[1]) << endl << endl;
    }
};

class Adaboost
{
  private:

    INT32 perceptron[INDEX_ada][HISTORY_LENGTH_ada];
    UINT32 fcount[2];
    UINT32 bcount[2];
    UINT32 count[2][2];
    UINT32 threshold;
    INT32 GHR[HISTORY_LENGTH_ada];
    UINT32 GHR_index;
    INT32 last_result[3];
    BOOL last_taken[3];
    UINT32 weight_threshold;

  public: 

    Adaboost()
    {

      threshold = log2(1.93*HISTORY_LENGTH_ada + 14);
      threshold = 1;

      weight_threshold = _pow(2,threshold);

      last_result[0] = 0;
      last_result[1] = 0;
      last_result[2] = 0;

      last_taken[0] = FALSE;
      last_taken[1] = FALSE;
      last_taken[2] = FALSE;

      GHR_index = 0;

      count[0][0] = 0;
      count[1][0] = 0;
      count[0][1] = 0;
      count[1][1] = 0;

      fcount[0] = 0;
      fcount[1] = 0;
      bcount[0] = 0;
      bcount[1] = 0;

      for(UINT32 i=0; i<HISTORY_LENGTH_ada; i++)
        {
        GHR[i] = 1-2*(i%2);
        }

      for(UINT32 i=0; i<INDEX_ada; i++)
      {
        for(UINT32 j=0; j<HISTORY_LENGTH_ada; j++)
        {
        perceptron[i][j] = 0;
        }
      }
    }
  
    bool predict(UINT32 PC)
    {
      INT32 result[3], _PC = PC, shift = 0;
      BOOL one_true = FALSE, two_true = FALSE;
      for (UINT32 j=0; j<3; j++){
        result[j] = 0;
        for (UINT32 i=0; i<HISTORY_LENGTH_ada; i++){
          result[j] += perceptron[shift+_PC%8][i]*GHR[(i+GHR_index)%HISTORY_LENGTH_ada];
        }
        _PC = PC/8;
        shift += 8;
        last_result[j] = result[j];
        last_taken[j] = FALSE;
        if (result[j] > 0){
          last_taken[j] = TRUE;
          if (one_true) two_true = TRUE;
          else one_true = TRUE;
        }
      }
      if (two_true){
        return TRUE;
      }
      return FALSE;
    }

    void update(UINT32 PC, bool pred, bool taken, bool forward)
    {
      UINT32 _PC = PC, shift = 0;
      for(UINT32 i=0; i<3; i++){
        if (taken != last_taken[i])
        // if (taken != pred || _abs(last_result) < threshold)
        {
          if (taken){
            for (UINT32 j=0; j<HISTORY_LENGTH_ada; j++){
              perceptron[shift+_PC%8][j] = _min(perceptron[shift+_PC%8][j] + GHR[(j+GHR_index)%HISTORY_LENGTH_ada], weight_threshold);
            }
          }
          else{
            for (UINT32 j=0; j<HISTORY_LENGTH_ada; j++){
              perceptron[shift+_PC%8][j] = _max(perceptron[shift+_PC%8][j] - GHR[(j+GHR_index)%HISTORY_LENGTH_ada], -weight_threshold);
            }
          }
        }
        _PC = PC/8;
        shift += 8;
      }
      if (taken==true) GHR[GHR_index%HISTORY_LENGTH_ada] = 1;
      else  GHR[GHR_index%HISTORY_LENGTH_ada] = -1;
      GHR_index++;

      count[(int)pred][(int)taken]++;

      if (forward && taken==pred) fcount[1]++;
      else if (forward && taken!=pred) fcount[0]++;
      else if (!forward && taken==pred) bcount[1]++;
      else bcount[0]++;
    }

    void print(ostream*& out)
    {
      *out << "Adaboost" << endl;
      *out << "INDEX = " << INDEX_ada << ", HISTORY_LENGTH = " << HISTORY_LENGTH_ada << ", weight_threshold = " << weight_threshold << endl;
      *out << "Pred\\Actual\tNT\tT" << endl;
      *out << "NT\t" << count[0][0] << "\t" << count[0][1] << endl;
      *out << "T \t" << count[1][0] << "\t" << count[1][1] << endl;
      *out << "Accuracy: " << (1.0*(count[0][0]+count[1][1]))/(count[0][0]+count[1][1]+count[1][0]+count[0][1]) << endl;
      *out << "Forward Mispredictions: " << (1.0*fcount[0])/(fcount[0]+fcount[1]) << endl;
      *out << "Backward Mispredictions: " << (1.0*bcount[0])/(bcount[0]+bcount[1]) << endl << endl;
    }
};