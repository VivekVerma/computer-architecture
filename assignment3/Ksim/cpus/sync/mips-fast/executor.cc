#include "executor.h"

Exe::Exe (Mipc *mc)
{
   _mc = mc;
}

Exe::~Exe (void) {}

void
Exe::MainLoop (void)
{
   unsigned int ins;
   Bool isSyscall, isIllegalOp;

   while (1) {
      AWAIT_P_PHI0;	// @posedge
      if (_mc->_eflag && _mc->_dready) {

         unsigned temp0 = _mc->_id_ex._decodedDST;
         unsigned temp1 = _mc->_id_ex._subregOperand;
         unsigned int temp2 = _mc->_id_ex._pc;
         unsigned int npc = _mc->_id_ex._npc;
         unsigned temp3 = _mc->_id_ex._writeREG;
         unsigned temp4 = _mc->_id_ex._writeFREG;
         unsigned temp5 = _mc->_id_ex._loWPort;
         unsigned temp6 = _mc->_id_ex._hiWPort;

         ins = _mc->_id_ex._ins;
         isSyscall = _mc->_id_ex._isSyscall;
         isIllegalOp = _mc->_id_ex._isIllegalOp;
         _mc->_ex_mem._overwrite_npc = FALSE;

         if (!isSyscall && !isIllegalOp) {
            _mc->_id_ex._opControl(_mc,ins);
#ifdef MIPC_DEBUG
            fprintf(_mc->_debugLog, "<%llu> Executed ins %#x\n", SIM_TIME, ins);
#endif
         }
         else if (isSyscall) {
#ifdef MIPC_DEBUG
            fprintf(_mc->_debugLog, "<%llu> Deferring execution of syscall ins %#x\n", SIM_TIME, ins);
#endif
            _mc->_eflag = FALSE;
         }
         else {
#ifdef MIPC_DEBUG
            fprintf(_mc->_debugLog, "<%llu> Illegal ins %#x in execution stage at PC %#x\n", SIM_TIME, ins, _mc->_pc);
#endif
         }

         if (!isIllegalOp && !isSyscall) {
            if (_mc->_lastbd && _mc->_btaken)
            {                                            //Performing the updating of the PC for the brach instruction in the +ve half
               npc = _mc->_btgt;                    //so that we have only one branch delay slot
               _mc->_ex_mem._overwrite_npc = TRUE;
            }
            _mc->_lastbd = _mc->_bd;
         }

         AWAIT_P_PHI1;	// @negedge

         _mc->_eready = TRUE;
         _mc->_decodeValid = FALSE;
         _mc->_execValid = TRUE;

         _mc->_ex_mem._npc = npc;

        _mc->_ex_mem._decodedDST = temp0;
        _mc->_ex_mem._subregOperand = temp1; // Use in Mem
        _mc->_ex_mem._pc = temp2;
        _mc->_ex_mem._isSyscall = isSyscall;
        _mc->_ex_mem._isIllegalOp = isIllegalOp;
        _mc->_ex_mem._MAR = _mc->_MAR;
        _mc->_ex_mem._writeREG = temp3;
        _mc->_ex_mem._writeFREG = temp4;
        _mc->_ex_mem._loWPort = temp5;
        _mc->_ex_mem._hiWPort = temp6;
        _mc->_ex_mem._ins = ins;

         // if (!isIllegalOp && !isSyscall) {
         //    if (_mc->_lastbd && _mc->_btaken)
         //    {
         //       _mc->_pc = _mc->_btgt;
         //    }
         //    else
         //    {
         //       _mc->_pc = _mc->_pc + 4;
         //    }
         //    _mc->_lastbd = _mc->_bd;
         // }
      }
      else {
         PAUSE(1);
      }
   }
}
