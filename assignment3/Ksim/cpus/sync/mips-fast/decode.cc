#include "decode.h"

Decode::Decode (Mipc *mc)
{
   _mc = mc;
}

Decode::~Decode (void) {}

void
Decode::MainLoop (void)
{
   unsigned int ins;
   while (1) {
      AWAIT_P_PHI0;	// @posedge
      if (!_mc->_Syscalworking && _mc->_fready) {
        unsigned int temp = _mc->_if_id._pc;
         ins = _mc->_if_id._ins;
         unsigned int npc = _mc->_if_id._npc;
         AWAIT_P_PHI1;	// @negedge
         _mc->_id_ex._pc = temp;
         _mc->_id_ex._npc = npc;
         _mc->Dec(ins);
         _mc->_id_ex._decodedSRC1 = _mc->_decodedSRC1;
         _mc->_id_ex._decodedSRC2 = _mc->_decodedSRC2;
         _mc->_id_ex._decodedDST = _mc->_decodedDST;
         _mc->_id_ex._subregOperand = _mc->_subregOperand;
         _mc->_id_ex._hi = _mc->_hi;
         _mc->_id_ex._lo = _mc->_lo;
         _mc->_id_ex._opControl = _mc->_opControl;
         _mc->_id_ex._memOp = _mc->_memOp;
         _mc->_id_ex._isIllegalOp = _mc->_isIllegalOp;
         _mc->_id_ex._writeREG = _mc->_writeREG;
         _mc->_id_ex._writeFREG = _mc->_writeFREG;
         _mc->_id_ex._loWPort = _mc->_loWPort;
         _mc->_id_ex._hiWPort = _mc->_hiWPort;
         _mc->_id_ex._ins = ins;
         if(_mc->_isSyscall)
         {
            _mc->_Syscalworking = TRUE;
            // _mc->_mpresent = TRUE;
            // _mc->_wpresent = TRUE;
         }
#ifdef MIPC_DEBUG
         fprintf(_mc->_debugLog, "<%llu> Decoded ins %#x\n", SIM_TIME, ins);
#endif
         _mc->_dready = TRUE;
         _mc->_insValid = FALSE;
         _mc->_decodeValid = TRUE;
      }
      else {
         PAUSE(1);
      }
   }
}
