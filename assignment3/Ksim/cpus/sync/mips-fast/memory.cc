#include "memory.h"

Memory::Memory (Mipc *mc)
{
   _mc = mc;
}

Memory::~Memory (void) {}

void
Memory::MainLoop (void)
{
   Bool memControl;

   while (1) {
      AWAIT_P_PHI0;	// @posedge
      if ((_mc->_mflag && _mc->_eready)) {
        unsigned temp1 = _mc->_ex_mem._decodedDST;
        unsigned temp2 = _mc->_ex_mem._opResultLo;
        unsigned temp3 = _mc->_ex_mem._opResultHi;
        unsigned temp4 = _mc->_ex_mem._pc;
        unsigned temp5 = _mc->_ex_mem._writeREG;
        unsigned temp6 = _mc->_ex_mem._writeFREG;
        unsigned temp7 = _mc->_ex_mem._loWPort;
        unsigned temp8 = _mc->_ex_mem._hiWPort;
         _mc->_mpresent = FALSE;
         memControl = _mc->_memControl;
         ins = _mc->_ex_mem._ins;
         AWAIT_P_PHI1;	// @negedge
         _mc->_mem_wb._decodedDST = temp1;
         _mc->_mem_wb._opResultLo = temp2;
         _mc->_mem_wb._opResultHi = temp3;
         _mc->_mem_wb._pc = temp4;
         _mc->_mem_wb._writeREG = temp5;
         _mc->_mem_wb._writeFREG = temp6;
         _mc->_mem_wb._loWPort = temp7;
         _mc->_mem_wb._hiWPort = temp8;
         _mc->_mem_wb._ins = ins;
         if(_mc->_isSyscall && !_mc->_eflag)
         {
            _mc->_mflag = FALSE;
         }
         if (memControl) {
            _mc->_ex_mem._memOp (_mc);
#ifdef MIPC_DEBUG
            fprintf(_mc->_debugLog, "<%llu> Accessing memory at address %#x for ins %#x\n", SIM_TIME, _mc->_MAR, _mc->_ins);
#endif
         }
         else {
#ifdef MIPC_DEBUG
            fprintf(_mc->_debugLog, "<%llu> Memory has nothing to do for ins %#x\n", SIM_TIME, _mc->_ins);
#endif
         }
         _mc->_mready = TRUE;
         _mc->_execValid = FALSE;
         _mc->_memValid = TRUE;

      }
      else {
         PAUSE(1);
      }
   }
}
