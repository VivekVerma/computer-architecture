#include "wb.h"

Writeback::Writeback (Mipc *mc)
{
   _mc = mc;
}

Writeback::~Writeback (void) {}

void
Writeback::MainLoop (void)
{
   unsigned int ins;
   Bool writeReg;
   Bool writeFReg;
   Bool loWPort;
   Bool hiWPort;
   Bool isSyscall;
   Bool isIllegalOp;
   unsigned decodedDST;
   unsigned opResultLo, opResultHi;

   while (1) {
      AWAIT_P_PHI0;	// @posedge
      // Sample the important signals
      if ((_mc->_mready)) {
         // _mc->_wpresent = FALSE;
         writeReg = _mc->_mem_wb._writeREG;
         writeFReg = _mc->_mem_wb._writeFREG;
         loWPort = _mc->_mem_wb._loWPort;
         hiWPort = _mc->_mem_wb._hiWPort;
         decodedDST = _mc->_mem_wb._decodedDST;
         opResultLo = _mc->_mem_wb._opResultLo;
         opResultHi = _mc->_mem_wb._opResultHi;
         isSyscall = _mc->_mem_wb._isSyscall;
         isIllegalOp = _mc->_mem_wb._isIllegalOp;
         ins = _mc->_mem_wb._ins;
         unsigned temp = _mc->_mem_wb._pc;

         if(!isSyscall && !isIllegalOp)
         {
            if (writeReg)
            {
               _mc->_gpr[decodedDST] = opResultLo;
   #ifdef MIPC_DEBUG
               fprintf(_mc->_debugLog, "<%llu> Writing to reg %u, value: %#x\n", SIM_TIME, decodedDST, opResultLo);
   #endif
            }

            else if (writeFReg)
            {
               _mc->_fpr[(decodedDST)>>1].l[FP_TWIDDLE^((decodedDST)&1)] = opResultLo;
   #ifdef MIPC_DEBUG
               fprintf(_mc->_debugLog, "<%llu> Writing to freg %u, value: %#x\n", SIM_TIME, decodedDST>>1, opResultLo);
   #endif
            }
         }

         AWAIT_P_PHI1;       // @negedge
         _mc->_wb_if._pc = temp;
         if (isSyscall) {
#ifdef MIPC_DEBUG
            fprintf(_mc->_debugLog, "<%llu> SYSCALL! Trapping to emulation layer at PC %#x\n", SIM_TIME, _mc->_pc);
#endif
            // _mc->_opControl(_mc, ins);
            _mc->fake_syscall(ins);
            //_mc->_pc += 4;
            _mc->_Syscalworking = FALSE;
            _mc->_fready = FALSE;
            _mc->_eready = FALSE;
            _mc->_dready = FALSE;
            _mc->_mready = FALSE;
            _mc->_eflag = TRUE;
            _mc->_mflag = TRUE;

         }
         else if (isIllegalOp) {
            printf("Illegal ins %#x at PC %#x. Terminating simulation!\n", ins, _mc->_pc);
#ifdef MIPC_DEBUG
            fclose(_mc->_debugLog);
#endif
            printf("Register state on termination:\n\n");
            _mc->dumpregs();
            exit(0);
         }
         else {
//             if (writeReg) {
//                _mc->_gpr[decodedDST] = opResultLo;
// #ifdef MIPC_DEBUG
//                fprintf(_mc->_debugLog, "<%llu> Writing to reg %u, value: %#x\n", SIM_TIME, decodedDST, opResultLo);
// #endif
//             }
//             else if (writeFReg) {
//                _mc->_fpr[(decodedDST)>>1].l[FP_TWIDDLE^((decodedDST)&1)] = opResultLo;
// #ifdef MIPC_DEBUG
//                fprintf(_mc->_debugLog, "<%llu> Writing to freg %u, value: %#x\n", SIM_TIME, decodedDST>>1, opResultLo);
// #endif
//             }
            if (loWPort || hiWPort) {
               if (loWPort) {
                  _mc->_lo = opResultLo;
#ifdef MIPC_DEBUG
                  fprintf(_mc->_debugLog, "<%llu> Writing to Lo, value: %#x\n", SIM_TIME, opResultLo);
#endif
               }
               if (hiWPort) {
                  _mc->_hi = opResultHi;
#ifdef MIPC_DEBUG
                  fprintf(_mc->_debugLog, "<%llu> Writing to Hi, value: %#x\n", SIM_TIME, opResultHi);
#endif
               }
            }
         }
         _mc->_gpr[0] = 0;
         _mc->_memValid = FALSE;
         _mc->_insDone = TRUE;
      }
      else {
         PAUSE(1);
      }
   }
}
